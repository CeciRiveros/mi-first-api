package com.folcademy.desafio.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostController {
    @PostMapping("/post")
    public String metodoPost(){
        return "Hola soy Post";
    }
   @GetMapping("/get")
    public String metodoGet(){
        return "Hola soy Get";
    }
}
